const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
// })

// txtLastName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
// })

// multiple listener
// txtFirstName.addEventListener('keyup', (event)=>{
// 	console.log(event.target);
// 	console.log(event.target.value)
// })

const updateFullName = () => {
	let fName = txtFirstName.value;
	let lName = txtLastName.value;
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener('keyup', updateFullName)
txtLastName.addEventListener('keyup', updateFullName)

